# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

"""
WSGI config for app project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings")
os.environ.setdefault("DJANGO_CONFIGURATION", "Production")
os.environ.setdefault("SCRAPY_SETTINGS_MODULE", "app.settings")

from configurations.wsgi import get_wsgi_application  # noqa: E402

application = get_wsgi_application()
