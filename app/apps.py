# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.apps import AppConfig


class CustomAppConfig(AppConfig):
    name = "app"

    def ready(self):
        from . import signals  # noqa: F401

        super().ready()


__all__ = ["CustomAppConfig"]
