# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel
from .faculty import Faculty


@reversion.register(ignore_duplicates=True)
class ProgramType(BaseModel):
    """This model represents the program type at a faculty."""

    code = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The identification code for this program type specified "
            "by the university.",
        ),
    )
    name = models.CharField(
        max_length=255,
        db_index=True,
        help_text=_(
            "The name of this program type (e.g. Bachelor's Programs).",
        ),
    )

    faculty = models.ForeignKey(
        Faculty,
        db_index=True,
        related_name="program_types",
        on_delete=models.PROTECT,
        help_text=_("The faculty of this program type."),
    )

    def __str__(self):
        """Return this program type's code and name."""
        return self.format_uri(self.code)


@reversion.register(ignore_duplicates=True)
class Program(BaseModel):
    """This model represents the program at a faculty."""

    code = models.CharField(
        max_length=255,
        unique=True,
        db_index=True,
        help_text=_(
            "The identification code for this program specified by the university.",
        ),
    )
    name = models.CharField(
        max_length=1023,
        db_index=True,
        help_text=_(
            "The name of this program "
            "(e.g. Joint Section Bachelor of Science in Engineering).",
        ),
    )

    type = models.ForeignKey(
        ProgramType,
        db_index=True,
        related_name="programs",
        on_delete=models.PROTECT,
        help_text=_("The type of this program."),
    )

    def __str__(self):
        """Return this program's code and name."""
        return self.format_uri(self.code)
