# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from .course import Course, CourseOffering
from .document import Document, DocumentRating, DocumentTag
from .employee import Employee
from .faculty import Faculty
from .program import Program, ProgramType
from .user import User

__all__ = [
    "Course",
    "Employee",
    "Faculty",
    "Program",
    "ProgramType",
    "CourseOffering",
    "Course",
    "Document",
    "DocumentTag",
    "DocumentRating",
    "User",
]
