# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from __future__ import annotations

import reversion
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel
from .employee import Employee
from .program import Program
from .user import User


@reversion.register(ignore_duplicates=True)
class Course(BaseModel):
    """This model represents a single course."""

    code = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The identification code for this course specified by the university.",
        ),
    )
    name = models.CharField(
        max_length=255,
        db_index=True,
        help_text=_("The name of this course."),
    )
    specifications_url = models.CharField(
        max_length=255,
        help_text=_("The link for the specifications of this course."),
    )
    content = models.TextField(
        help_text=_("The markdown formatted content of this course."),
    )

    programs = models.ManyToManyField(
        Program,
        through="app.CourseOffering",
        related_name="courses",
        blank=True,
        help_text=_("The programs where this course is taught at."),
    )

    subscriptions = models.ManyToManyField(
        User,
        related_name="subscriptions",
        blank=True,
        help_text=_("The users that subscribed to this course."),
    )

    @property
    def uri(self) -> str:
        """Return a unique resource identifier that identifies this course."""
        return self.format_uri("ghent-university", self.code)


@reversion.register(ignore_duplicates=True)
class CourseOffering(BaseModel):
    """This model represents the relation between courses and programs."""

    program = models.ForeignKey(
        Program,
        related_name="offerings",
        on_delete=models.CASCADE,
        help_text=_("The program where the course is offered."),
    )
    course = models.ForeignKey(
        Course,
        related_name="offerings",
        on_delete=models.CASCADE,
        help_text=_("The course that is offered."),
    )

    code = models.CharField(
        max_length=1,
        blank=True,
        help_text=_(
            "The identification code for this course offering or teaching method "
            "specified by the university (e.g. A or B).",
        ),
    )
    reference = models.CharField(
        max_length=1,
        blank=True,
        help_text=_(
            "An extra reference specified with this offering by the university"
        ),
    )
    # TODO: Choices (?, 1, 2 or NVT)?
    semester = models.CharField(
        max_length=3,
        blank=True,
        help_text=_("The semester of the year that the course is taught at."),
    )
    year = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(3)],
        help_text=_(
            "Full-time trajectory. This field shows which model track (master year) "
            "a course belongs to. 1 = 1st master year, 2 = 2nd master year, "
            "not filled in = the student chooses when to take the course."
        ),
    )
    year_part_time = models.IntegerField(
        blank=True,
        null=True,
        validators=[MinValueValidator(1), MaxValueValidator(3)],
        help_text=_(
            "Part-time trajectory. This field shows how a student can spread the study "
            "program over several academic years. This is intended for students who "
            "want to study part-time (eg in combination with work). If a student "
            "follows a GIT then he/she doesn't have to take this field into account."
        ),
    )
    department = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("The department the course is taught at."),
    )
    instructor = models.ForeignKey(
        Employee,
        blank=True,
        null=True,
        related_name="course_relations",
        on_delete=models.CASCADE,
        help_text=_("The instructor that teaches the course."),
    )
    contact_hours = models.IntegerField(
        default=False,
        help_text=_(
            "The nominal amount of contact time for this course.",
        ),
    )
    study_hours = models.IntegerField(
        default=False,
        help_text=_(
            "The nominal amount of study time for this course.",
        ),
    )
    credits = models.DecimalField(
        blank=True,
        null=True,
        max_digits=3,
        decimal_places=1,
        default=False,
        help_text=_(
            "The amount of credits",
        ),
    )

    def __str__(self):
        """Return this relation's course and tag."""
        return self.format_uri(self.program, self.course)


__all__ = ["Course", "CourseOffering"]
