# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import reversion
from django.db import models
from django.utils.translation import gettext_lazy as _

from .base import BaseModel


@reversion.register(ignore_duplicates=True)
class Faculty(BaseModel):
    """This model represents the faculty a course is taught at."""

    code = models.CharField(
        max_length=255,
        unique=True,
        help_text=_(
            "The identification code for this faculty specified by the university "
            "in the course catalogue.",
        ),
    )
    human_code = models.CharField(
        max_length=255,
        help_text=_(
            "The abbreviation code for this faculty used by students "
            "and the university's main website.",
        ),
    )
    legacy_code = models.CharField(
        max_length=255,
        help_text=_(
            "The old abbreviation code for this faculty used in CAS "
            "and the university's phone book.",
        ),
    )
    name = models.CharField(
        max_length=255,
        help_text=_(
            "The name of this faculty (e.g. Faculty of Engineering and Architecture).",
        ),
    )

    def __str__(self):
        """Return this faculty's code and name."""
        return f"{self.__class__.__name__}: {self.name}"

    class Meta:
        verbose_name_plural = "Faculties"
