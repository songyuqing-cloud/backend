# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from __future__ import annotations

from datetime import datetime

import reversion
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


@reversion.register(ignore_duplicates=True)
class User(AbstractUser):
    """This model represents a single user.

    This model extends the base Django User model with specific UGent CAS
    attributes.
    """

    faculty = models.CharField(
        max_length=255,
        help_text=_("The faculty the student is enrolled at."),
    )
    university_id = models.CharField(
        max_length=255,
        help_text=_("The unique id given to a student by the university."),
    )
    last_enrolled = models.IntegerField(
        default=datetime.min.year,
        blank=True,
        null=True,
        help_text=_("The last year a student was enrolled at the university. "),
    )

    @property
    def name(self):
        return str(self.first_name + self.last_name)

    class Meta:
        db_table = "auth_user"


__all__ = ["User"]
