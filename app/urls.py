# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import RedirectView
from filebrowser.sites import site as filebrowser_site
from graphene_file_upload.django import FileUploadGraphQLView

from app.views import SAMLValidateMock

urlpatterns = [
    # Admin
    path("admin/filebrowser/", filebrowser_site.urls),
    path("admin/", admin.site.urls, name="admin"),
    path("grappelli/", include("grappelli.urls"), name="grappelli"),
    re_path(r"^markdownx/", include("markdownx.urls")),
    # Project
    path(
        "graphql/",
        FileUploadGraphQLView.as_view(graphiql=settings.DEBUG),
        name="graphql",
    ),
]

if settings.DEBUG:
    urlpatterns += [
        path("", RedirectView.as_view(url="graphql"), name="index"),
        path(
            "samlValidate",
            SAMLValidateMock.as_view(),
            name="saml-validate",
        ),
    ]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
else:
    urlpatterns += [
        path("sentry-debug/", lambda request: 1 / 0, name="sentry-debug"),
    ]
