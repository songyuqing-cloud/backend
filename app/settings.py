# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from datetime import timedelta
from pathlib import Path

import dj_database_url
import django_cache_url
from configurations import Configuration, values
from django.utils.translation import ugettext_lazy as _


class Base(Configuration):
    """Base configuration that defines settings that are shared between environments."""

    DOMAIN = values.Value("studium.ugent.be")

    BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
    APP_DIR = BASE_DIR / "app"

    FIXTURE_DIRS = [APP_DIR / "fixtures"]

    MEDIA_ROOT = BASE_DIR / "media"
    MEDIA_URL = values.Value("/media/")

    STATIC_ROOT = BASE_DIR / "static"
    STATIC_URL = values.Value("/static/")

    SECRET_KEY = values.Value("otdzafDAmCy9coCkprTSmMnBcBn9nsFaxeEJec48", environ=False)

    DEBUG = values.BooleanValue(True, environ=False)

    ALLOWED_HOSTS = [".backend", ".localhost", "127.0.0.1", "[::1]"]

    # Application definition
    INSTALLED_APPS = [
        # Admin and Django apps
        "grappelli",
        "filebrowser",
        "django.contrib.admin",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        # Other apps
        "django_extensions",
        "django_filters",
        "graphene_django",
        "graphql_jwt.refresh_token.apps.RefreshTokenConfig",
        "reversion",
        "post_office",
        # Project apps
        "app",
        # Other apps
        "markdownx",
    ]

    MIDDLEWARE = [
        # Django middleware
        "django.middleware.security.SecurityMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
        # Other middleware
        "reversion.middleware.RevisionMiddleware",
    ]

    # Url configuration
    ROOT_URLCONF = "app.urls"

    TEMPLATES = [
        {
            "BACKEND": "django.template.backends.django.DjangoTemplates",
            "DIRS": [BASE_DIR / "templates"],
            "APP_DIRS": True,
            "OPTIONS": {
                "context_processors": [
                    "django.template.context_processors.debug",
                    "django.template.context_processors.request",
                    "django.contrib.auth.context_processors.auth",
                    "django.contrib.messages.context_processors.messages",
                ],
            },
        },
    ]

    WSGI_APPLICATION = "app.wsgi.application"

    # Database
    DATABASES = values.DatabaseURLValue(f"sqlite:///{BASE_DIR / 'db.sqlite3'}")

    # Custom user model
    AUTH_USER_MODEL = "app.User"

    # Authentication backends
    AUTHENTICATION_BACKENDS = [
        "graphql_jwt.backends.JSONWebTokenBackend",
        "django.contrib.auth.backends.ModelBackend",
    ]

    # Password validation
    AUTH_PASSWORD_VALIDATORS = [
        {
            "NAME": (
                "django.contrib.auth.password_validation"
                ".UserAttributeSimilarityValidator"
            ),
        },
        {
            "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
        },
    ]

    # Email configuration
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
    EMAIL_HOST = "localhost"
    DEFAULT_FROM_EMAIL = "studium@vtk.ugent.be"
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True

    # Internationalization
    LOCALE_PATHS = [APP_DIR / "locale"]
    LANGUAGE_CODE = values.Value("nl-be", environ=False)
    LANGUAGES = (
        ("nl-be", _("Dutch")),
        ("en-us", _("English")),
    )
    TIME_ZONE = values.Value("Europe/Brussels", environ=False)
    USE_I18N = values.BooleanValue(True, environ=False)
    USE_L10N = values.BooleanValue(True, environ=False)
    USE_TZ = values.BooleanValue(True, environ=False)

    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "handlers": {"console": {"class": "logging.StreamHandler"}},
        "loggers": {"main": {"handlers": ["console"], "level": "DEBUG"}},
    }

    # Grappelli configuration
    GRAPPELLI_ADMIN_TITLE = values.Value("Admin", environ=False)
    GRAPPELLI_SWITCH_USER = values.BooleanValue(True, environ=False)
    GRAPPELLI_CLEAN_INPUT_TYPES = values.BooleanValue(True, environ=False)

    # CAS Configuration
    CAS_SERVER = values.Value("http://login.ugent.be", environ=False)

    # GraphQL configuration
    GRAPHENE = {
        "SCHEMA": "app.schema.schema",
        "SCHEMA_INDENT": 2,
        "MIDDLEWARE": (
            "graphql_jwt.middleware.JSONWebTokenMiddleware",
            "graphene_django.debug.DjangoDebugMiddleware",
        ),
    }

    GRAPHQL_JWT = {
        # Add information about our application to the token
        "JWT_AUDIENCE": "Students",
        # Use Bearer as the token prefix
        "JWT_AUTH_HEADER_PREFIX": "Bearer",
        # Use a long running refresh token and verify expiry
        "JWT_VERIFY_EXPIRATION": True,
        "JWT_ALLOW_REFRESH": True,
        "JWT_LONG_RUNNING_REFRESH_TOKEN": True,
        # Always regenerate refresh tokens
        "JWT_REUSE_REFRESH_TOKENS": False,
        # Use a long refresh token (default is 20)
        "JWT_REFRESH_TOKEN_N_BYTES": 64,
        # Set the expiration time to 15 minutes for tokens and 7 days for refresh tokens
        "JWT_EXPIRATION_DELTA": timedelta(minutes=10),
        "JWT_REFRESH_EXPIRATION_DELTA": timedelta(days=7),
        # Still validate tokens that expired less than five seconds ago
        "JWT_LEEWAY": timedelta(seconds=5),
    }

    MARKDOWNX_EDITOR_RESIZABLE = True
    MARKDOWNX_MARKDOWN_EXTENSIONS = ["markdown.extensions.extra"]

    # Scrapy configuration
    BOT_NAME = "scraper"

    SPIDER_MODULES = ["scraper.spiders"]
    NEWSPIDER_MODULE = "scraper.spiders"

    # TODO: set True after we get permission from https://studiegids.ugent.be/robots.txt
    ROBOTSTXT_OBEY = False

    ITEM_PIPELINES = {
        "scraper.pipelines.CoursePipeline": 300,
        "scraper.pipelines.FacultyPipeline": 200,
    }

    @classmethod
    def setup(cls):
        super().setup()

        # Add domain to allowed hosts
        cls.ALLOWED_HOSTS.append(f".{cls.DOMAIN}")

        # Set CAS service to domain
        cls.CAS_SERVICE = f"https://{cls.DOMAIN}"

        # Set JWT issuer to domain
        cls.GRAPHQL_JWT["JWT_ISSUER"] = cls.DOMAIN

        # Add domain to scrapy User Agent
        cls.USER_AGENT = f"scraper (+https://{cls.DOMAIN})"


class Local(Base):
    """Local configuration for when the app is run local (non-dockerized)."""

    # CAS configuration
    CAS_SERVER = values.Value("http://localhost:5000")


class Container(Base):
    """Container configuration for when the app is run in a container (dockerized)."""

    DATABASES = values.DatabaseURLValue(
        "postgres://postgres:postgres@database:5432/studium",
        environ=False,
    )

    CACHES = values.CacheURLValue("redis://cache:6379", environ=False)


class Development(Container):
    """Development configuration for when the app is run in development (dockerized)."""

    # Install CORS middleware
    INSTALLED_APPS = Container.INSTALLED_APPS + ["corsheaders"]
    MIDDLEWARE = ["corsheaders.middleware.CorsMiddleware"] + Container.MIDDLEWARE

    # Disable CORS checks
    CORS_ORIGIN_ALLOW_ALL = True

    # CAS configuration
    CAS_SERVER = values.Value("http://localhost")

    @classmethod
    def post_setup(cls):
        super().post_setup()

        # Disable CSRF checks
        cls.MIDDLEWARE.remove("django.middleware.csrf.CsrfViewMiddleware")


class Testing(Container):
    """Testing configuration for when the app is tested (dockerized)."""

    DEBUG = values.BooleanValue(False, environ=False)

    # Test runner
    TEST_RUNNER = "xmlrunner.extra.djangotestrunner.XMLTestRunner"
    TEST_OUTPUT_DIR = "out"
    TEST_OUTPUT_FILE_NAME = "unittest.xml"

    # Disable logging for speed
    LOGGING = {"version": 1, "disable_existing_loggers": True}

    # Use a single fast password hashing algorithm
    PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]


class Production(Container):
    """Production configuration for when the app is run in production (dockerized)."""

    STATICFILES_STORAGE = "app.storage.ForgivingManifestStaticFilesStorage"

    DEBUG = values.BooleanValue(False, environ=False)
    USE_ETAGS = values.BooleanValue(True, environ=False)

    SECRET_KEY = values.SecretValue()

    ALLOWED_HOSTS = [".backend", ".localhost", ".next_backend", ".studium.gent"]

    REDIS_HOST = values.Value("cache")
    REDIS_PORT = values.Value(6379)

    POSTGRES_HOST = values.Value("database")
    POSTGRES_PORT = values.Value(5432)
    POSTGRES_DB = values.Value("postgres")
    POSTGRES_USER = values.Value("postgres")
    POSTGRES_PASS = values.SecretValue()

    # GSuite email configuration
    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    EMAIL_HOST = "smtp-relay.gmail.com"
    EMAIL_HOST_USER = "surfin.server@vtk.ugent.be"
    EMAIL_HOST_PASSWORD = values.SecretValue()
    DEFAULT_FROM_EMAIL = "site@vtk.ugent.be"
    SERVER_EMAIL = "studium.server@vtk.ugent.be"
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True

    # G Suite has a sending limit of 500 recipients
    # https://support.google.com/a/answer/166852?hl=en
    G_SUITE_RECIPIENTS_PER_MESSAGE_SENT_VIA_SMTP_LIMIT = 100

    # HTTPS settings
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True

    SECURE_BROWSER_XSS_FILTER = True
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True
    SECURE_HSTS_SECONDS = 60
    SECURE_SSL_REDIRECT = True

    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

    # Sentry configuration
    SENTRY_DSN = values.SecretValue()

    @classmethod
    def post_setup(cls):
        import sentry_sdk
        from sentry_sdk.integrations.django import DjangoIntegration

        super().post_setup()

        # Add default databases connection
        pg_cred = f"{cls.POSTGRES_USER}:{cls.POSTGRES_PASS}"
        pg_conn = f"{cls.POSTGRES_HOST}:{cls.POSTGRES_PORT}/{cls.POSTGRES_DB}"
        cls.DATABASES["default"] = dj_database_url.parse(
            f"postgres://{pg_cred}@{pg_conn}"
        )
        cls.CACHES["default"] = django_cache_url.parse(
            f"redis://{cls.REDIS_HOST}:{cls.REDIS_PORT}"
        )

        # Initialize Sentry
        sentry_sdk.init(
            dsn=cls.SENTRY_DSN,
            integrations=[DjangoIntegration()],
            send_default_pii=True,
        )
