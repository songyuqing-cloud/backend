# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import factory.fuzzy
from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory


class UserFactory(DjangoModelFactory):
    class Meta:
        model = get_user_model()
        django_get_or_create = ("username",)

    class Params:
        profile = factory.Faker("simple_profile")

    username = factory.LazyAttribute(lambda u: u.profile["username"])
    password = factory.Faker("password")

    first_name = factory.LazyAttribute(lambda u: u.profile["name"].split(" ", 1)[0])
    last_name = factory.LazyAttribute(lambda u: u.profile["name"].split(" ", 1)[1])
    email = factory.LazyAttribute(lambda u: u.profile["mail"])

    is_superuser = False
    is_staff = False
    is_active = True
