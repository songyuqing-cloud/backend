# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from graphql_jwt.exceptions import PermissionDenied

from app.factories.course import CourseFactory
from app.factories.user import UserFactory
from app.tests.base import BaseTestCase


class CourseByCodeTestCase(BaseTestCase):
    QUERY = 'query { courseByCode(code: "%s") { code, name } }'

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.course = CourseFactory()

    def test_course_by_code_denied(self):
        result = self.client.execute(self.QUERY % self.course.code)
        self.assertEqual(len(result.errors), 1)
        self.assertEqual(result.errors[0].message, PermissionDenied.default_message)

    def test_course_by_code_none(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY % "X000000")
        self.assertEqual(result.errors, None)
        self.assertEqual(result.data["courseByCode"], None)

    def test_course_by_code(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY % self.course.code)
        self.assertEqual(result.errors, None)
        item = result.data["courseByCode"]
        self.assertEqual(item["code"], self.course.code)
        self.assertEqual(item["name"], self.course.name)


class CourseByNameTestCase(BaseTestCase):
    QUERY = 'query { courseByName(name: "%s") { code, name } }'

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.course = CourseFactory()

    def test_course_by_name_denied(self):
        result = self.client.execute(self.QUERY % self.course.name)
        self.assertEqual(len(result.errors), 1)
        self.assertEqual(result.errors[0].message, PermissionDenied.default_message)

    def test_course_by_name_none(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY % "Some name of a course.")
        self.assertEqual(result.errors, None)
        self.assertEqual(result.data["courseByName"], None)

    def test_course_by_name(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY % self.course.name)
        self.assertEqual(result.errors, None)
        item = result.data["courseByName"]
        self.assertEqual(item["code"], self.course.code)
        self.assertEqual(item["name"], self.course.name)


class AllCoursesTestCase(BaseTestCase):
    QUERY = "query { allCourses { edges { node { code } } } }"

    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory()
        cls.courses = [CourseFactory() for _ in range(20)]

    def test_all_courses_denied(self):
        result = self.client.execute(self.QUERY)
        self.assertEqual(len(result.errors), 1)
        self.assertEqual(result.errors[0].message, PermissionDenied.default_message)

    def test_all_courses(self):
        self.client.authenticate(self.user)
        result = self.client.execute(self.QUERY)
        self.assertEqual(result.errors, None)
        self.assertEqual(len(result.data["allCourses"]["edges"]), len(self.courses))
