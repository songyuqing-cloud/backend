# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.core.management.base import BaseCommand
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scraper.spiders import CourseSpider
from scraper.spiders.faculty import FacultySpider


class Command(BaseCommand):
    help = "You get data! And you get data! Everybody gets data!"

    def handle(self, *args, **options):
        process = CrawlerProcess(get_project_settings())
        process.crawl(FacultySpider)
        process.crawl(CourseSpider)
        process.start()
