# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.contrib import admin
from reversion.admin import VersionAdmin

from app.models import Document, DocumentTag, DocumentRating


@admin.register(Document)
class DocumentAdmin(VersionAdmin):
    pass


@admin.register(DocumentTag)
class DocumentTagAdmin(VersionAdmin):
    pass


@admin.register(DocumentRating)
class DocumentRatingAdmin(VersionAdmin):
    pass
