# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
from django.contrib import admin
from django.db import models
from markdownx.widgets import AdminMarkdownxWidget
from reversion.admin import VersionAdmin

from app.models import Course, CourseOffering


class CourseOfferingInline(admin.StackedInline):
    model = CourseOffering
    extra = 0


@admin.register(Course)
class CourseAdmin(VersionAdmin):
    filter_horizontal = ["programs", "subscriptions"]
    list_display = ("code", "name")
    search_fields = ("code", "name")
    list_filter = ("programs",)
    inlines = [CourseOfferingInline]
    formfield_overrides = {models.TextField: {"widget": AdminMarkdownxWidget}}
