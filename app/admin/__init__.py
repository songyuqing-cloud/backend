# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from reversion.admin import VersionAdmin

from ..models import CourseOffering, Employee, Faculty, Program, ProgramType, User
from .course import CourseAdmin  # noqa: F401
from .document import DocumentAdmin  # noqa: F401

admin.site.register(Faculty, VersionAdmin)
admin.site.register(ProgramType, VersionAdmin)
admin.site.register(Program, VersionAdmin)
admin.site.register(Employee, VersionAdmin)
admin.site.register(CourseOffering, VersionAdmin)


@admin.register(User)
class CustomUserAdmin(VersionAdmin, UserAdmin):
    pass


__all__ = []
