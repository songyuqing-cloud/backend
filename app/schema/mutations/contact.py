import re

import graphene
from post_office import mail

from app.utils.error_handling import studium_clean


class ContactSendMutation(graphene.Mutation):
    """Mutation that will send an email with the given contact form information."""

    class Arguments:
        name = graphene.String(required=True)
        email = graphene.String(required=True)
        subject = graphene.String(required=True)
        message = graphene.String(required=True)

    ok = graphene.Boolean()

    @classmethod
    def clean_name(cls, name):
        # Check if the name exists
        if not name:
            return "Name is required"

        # Check if the name length is within the limits
        if len(name) > 100:
            return "Name cannot be larger than 100 characters"

    @classmethod
    def clean_email(cls, email):
        # Check if the email exists
        if not email:
            return "Email is required"

        # Check if the email length is within the limits
        # This is the recommended limit by the RFC:
        # https://datatracker.ietf.org/doc/html/rfc3696#section-3
        if len(email) > 256:
            return "Email cannot be larger than 256 characters"

        # Check if the email is valid
        if not re.match(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b", email):
            return "E-mail must be valid"

    @classmethod
    def clean_subject(cls, subject):
        # Check if the subject exists
        if not subject:
            return "Subject is required"

        # Check if the subject length is within the limits
        if len(subject) < 4:
            return "Subject must be at least 4 characters"
        if len(subject) > 100:
            return "Subject cannot be larger than 100 characters"

    @classmethod
    def clean_message(cls, message):
        # Check if the message exists
        if not message:
            return "Message is required"

        # Check if the message length is within the limits
        if len(message) < 10:
            return "Message must be at least 10 characters"
        if len(message) > 1000:
            return "Message cannot be larger than 1000 characters"

    @classmethod
    def mutate(cls, parent, info, name: str, email: str, subject: str, message: str):

        # Checks every clean-function and raises corresponding errors
        studium_clean(ContactSendMutation, **locals())

        # Send email
        mail.send(
            "contact@studium.gent",
            email,
            template="contact",
            context={
                "name": name,
                "email": email,
                "subject": subject,
                "message": message,
            },
        )

        return ContactSendMutation(ok=True)


class ContactMutation(graphene.ObjectType):
    send_contact = ContactSendMutation.Field()
