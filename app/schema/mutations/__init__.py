# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene

from app.schema.mutations.auth import AuthMutation
from app.schema.mutations.contact import ContactMutation
from app.schema.mutations.course import CourseMutation
from app.schema.mutations.document import DocumentMutation


class Mutation(
    AuthMutation, CourseMutation, DocumentMutation, ContactMutation, graphene.ObjectType
):
    pass
