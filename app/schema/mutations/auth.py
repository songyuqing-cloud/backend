# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
import graphql_jwt
from cas import CASClientWithSAMLV1
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from graphene.utils.thenables import maybe_thenable
from graphql_jwt import exceptions, signals
from graphql_jwt.decorators import (
    csrf_rotation,
    on_token_auth_resolve,
    refresh_expiration,
    setup_jwt_cookie,
)
from graphql_jwt.mixins import ObtainJSONWebTokenMixin as ObtainJWTMixin
from graphql_jwt.mixins import ResolveMixin

from app.models import User


class CASObtainJSONWebToken(ObtainJWTMixin, ResolveMixin, graphene.Mutation):
    """Mutation that obtains a JSON web token for subsequent requests.

    This mutation validates a CAS ticket granted by UGent and authenticated the
    user if it is valid. The user it's CAS attributes will be updated. When the
    user does not exist a new user is created.
    """

    class Arguments:
        ticket = graphene.String()

    @classmethod
    @setup_jwt_cookie
    @csrf_rotation
    @refresh_expiration
    def mutate(cls, root, info, **kwargs):
        """Verify the CAS ticket and resolve the query if it is valid."""
        context = info.context
        context._jwt_token_auth = True

        ticket = kwargs.pop("ticket")
        cas = CASClientWithSAMLV1(
            service_url=settings.CAS_SERVICE,
            server_url=settings.CAS_SERVER,
        )

        username, attributes, pgtiou = cas.verify_ticket(ticket)
        if not username:
            raise exceptions.JSONWebTokenError(
                _("Please enter valid credentials"),
            )

        # Create user if user does not exist
        user = User.objects.filter(username=username).first()
        if not user:
            user = User.objects.create_user(
                username=username,
                email=attributes.get("mail"),
            )

        # Update name
        user.first_name = attributes.get("givenname")
        user.last_name = attributes.get("surname")

        # Update UGent info
        user.university_id = attributes.get("ugentID")
        user.faculty = attributes.get("faculty")
        user.last_enrolled = int(
            next(
                iter(attributes.get("lastenrolled", "").split("-")),
                0,
            ),
        )

        # Save user model
        user.save()

        if hasattr(context, "user"):
            context.user = user

        result = cls.resolve(root, info, **kwargs)
        signals.token_issued.send(sender=cls, request=context, user=user)
        return maybe_thenable((context, user, result), on_token_auth_resolve)


class AuthMutation(graphene.ObjectType):
    cas_token_auth = CASObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    revoke_token = graphql_jwt.Revoke.Field()
