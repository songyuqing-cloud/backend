# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
from django.utils import timezone
from graphene import Boolean, Field, Mutation, String
from reversion import create_revision, set_comment, set_user

from app.models import Course
from app.schema.queries import CourseType


class CourseUpdateContentMutation(Mutation):
    class Arguments:
        content = String(required=True)
        code = String()

    course = Field(CourseType)

    @classmethod
    def mutate(cls, parent, info, content, code):
        course = Course.objects.filter(code=code).first()
        # TODO: Throw error?
        if not course:
            return course

        with create_revision():
            course.content = content
            course.save()

            set_user(info.context.user)
            now = timezone.now()
            set_comment(
                f"By {info.context.user.name} on {now.date()} "
                f"at {now.time().isoformat('seconds')}"
            )
        return CourseUpdateContentMutation(course=course)


class CourseUpdateSubscriptionMutation(Mutation):
    class Arguments:
        value = Boolean(required=True)
        code = String()

    course = Field(CourseType)

    @classmethod
    def mutate(cls, parent, info, value, code):
        user = getattr(info.context, "user", None)
        course = Course.objects.filter(code=code).first()

        # TODO: Throw error?
        if not course:
            return CourseUpdateSubscriptionMutation(value=(not value))

        with create_revision():
            if value:
                course.subscriptions.add(user)
            else:
                course.subscriptions.remove(user)

            course.save()
            set_user(user)
            now = timezone.now()
            set_comment(
                f"{user.name} subscribed to course {code} "
                f"on {now.date()} at {now.time().isoformat('seconds')}"
            )
        return CourseUpdateSubscriptionMutation(course=course)


class CourseMutation(graphene.ObjectType):
    update_course_content = CourseUpdateContentMutation.Field()
    update_course_subscription = CourseUpdateSubscriptionMutation.Field()


__all__ = ["CourseMutation"]
