# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import re

import graphene
import reversion
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
from graphene_file_upload.scalars import Upload
from PyPDF2 import PdfFileReader

from app.models import Course, Document, DocumentTag
from app.schema.queries.document import DocumentType
from app.utils.error_handling import studium_clean


class DocumentUploadMutation(graphene.Mutation):
    class Arguments:
        course_code = graphene.String(required=True)
        name = graphene.String(required=True)
        file = Upload(required=True)
        tags = graphene.List(graphene.String)

    document = graphene.Field(DocumentType)

    @classmethod
    def clean_course_code(cls, course_code):
        # Check if the course_code belongs to an existing course
        if not Course.objects.filter(code=course_code).first():
            return "This course doesn't exist!"

    @classmethod
    def clean_name(cls, name):
        # Check if the name exists
        if not name:
            return "The name is a required field. "

    @classmethod
    def clean_file(cls, file):
        # Check if a file was uploaded
        if not file or not file.file:
            return "It is required to upload a file. "

        # Check if filetype is valid
        allowed_type = False
        for extension in Document.document_extensions:
            if re.match(extension, file.content_type):
                allowed_type = True

        if not allowed_type:
            return (
                "This is not a valid filetype. The allowed extensions are "
                + ", ".join(Document.document_extensions)
                + "."
            )

        # TODO: check if file already exists? (hash)

    @classmethod
    def clean_tags(cls, tags):
        # Check if all uploaded tags are valid
        for tag in tags:
            if not DocumentTag.objects.filter(name=tag):
                return f"'{tag}' is not an existing tag."

    @classmethod
    def clean_course_code_name(cls, variables):
        if Document.objects.filter(
            name=variables["name"],
            course=Course.objects.filter(code=variables["course_code"]).first(),
        ).exists():
            return (
                'The course with course code \\"'
                + variables["course_code"]
                + '\\" already has a document with name \\"'
                + variables["name"]
                + '\\". '
            )

    @classmethod
    def clean_document_path(cls, variables):
        if (
            not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            or not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            .type.faculty
        ):
            return "This course isn't currently offered at any faculty. "

        if (
            not Course.objects.filter(code=variables["course_code"])
            .first()
            .programs.first()
            .type.faculty.human_code
        ):
            return (
                "The information of the faculty associated with this course could "
                "not be retrieved. "
            )

    @classmethod
    def mutate(
        cls,
        parent,
        info,
        course_code: str,
        name: str,
        file: InMemoryUploadedFile,
        tags: str,
        **kwargs,
    ):
        # Checks every clean-function and raises corresponding errors
        studium_clean(cls, **locals())

        # Get the course
        course = Course.objects.filter(code=course_code).first()

        # Get the current user
        user = info.context.user

        # Create a new document and make a first revision
        with reversion.create_revision():
            document = Document.objects.create(
                course=course,
                author=user,
                name=name,
                file=file,
                # TODO: Calculate or specify? PDF's calculated, other files usefull?
                page_count=0,
            )
            for tag in tags:
                document.tags.add(DocumentTag.objects.filter(name=tag).first())

            # Load the pdf to the PdfFileReader object with default settings
            if file.content_type == "application/pdf":
                with file.open("rb") as pdf_file:
                    pdf_reader = PdfFileReader(pdf_file)
                    document.page_count = pdf_reader.numPages
                document.save()

            # Add meta-information.
            reversion.set_user(user)
            # TODO: Sensible comment
            reversion.set_comment("Created revision 1")
        return DocumentUploadMutation(document=document)


class DocumentEditRatingMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        rating = graphene.Int(required=True)

    document = graphene.Field(DocumentType)

    @classmethod
    def mutate(
        cls,
        parent,
        info,
        id,
        rating,
    ):
        # Get the course
        document = Document.objects.filter(pk=id).first()

        # TODO: Throw error?
        if not document:
            return document

        # Get the current user
        user = info.context.user

        # Create a new document and make a first revision
        with reversion.create_revision():
            documentrating = document.document_ratings.filter(user_id=user.id).first()
            if documentrating:
                documentrating.rating = rating
                documentrating.save()
            else:
                document.ratings.add(user, through_defaults={"rating": rating})
                document.save()

            # Add meta-information.
            reversion.set_user(user)
            # TODO: Sensible comment
            now = timezone.now()
            reversion.set_comment(
                f"By {info.context.user.name} on {now.date()} "
                f"at {now.time().isoformat('seconds')}"
            )
        return DocumentEditRatingMutation(document=document)


class DocumentMutation(graphene.ObjectType):
    upload_document = DocumentUploadMutation.Field()
    update_rating = DocumentEditRatingMutation.Field()
