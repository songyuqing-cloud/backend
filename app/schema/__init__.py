# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene

from .mutations import Mutation
from .queries import (
    CourseOfferingType,
    CourseType,
    EmployeeType,
    FacultyType,
    ProgramType,
    ProgramTypeType,
    Query,
    UserType,
)

types = [
    CourseType,
    CourseOfferingType,
    EmployeeType,
    FacultyType,
    ProgramType,
    ProgramTypeType,
    UserType,
]
# noinspection PyTypeChecker
schema = graphene.Schema(query=Query, mutation=Mutation, types=types)

__all__ = ["schema"]
