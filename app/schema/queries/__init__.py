# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from graphene import ObjectType

from .course import CourseQuery, CourseType
from .course_offering import CourseOfferingQuery, CourseOfferingType
from .document import DocumentExtensionsQuery, DocumentQuery
from .document_rating import DocumentRatingQuery
from .document_tag import DocumentTagQuery
from .employee import EmployeeQuery, EmployeeType
from .faculty import FacultyQuery, FacultyType
from .program import ProgramQuery, ProgramType
from .program_type import ProgramTypeQuery, ProgramTypeType
from .user import UserQuery, UserType


class Query(
    CourseQuery,
    CourseOfferingQuery,
    DocumentExtensionsQuery,
    DocumentRatingQuery,
    DocumentTagQuery,
    DocumentQuery,
    EmployeeQuery,
    FacultyQuery,
    ProgramQuery,
    ProgramTypeQuery,
    UserQuery,
    ObjectType,
):
    pass


__all__ = [
    "CourseType",
    "CourseOfferingType",
    "EmployeeType",
    "FacultyType",
    "ProgramType",
    "ProgramTypeType",
    "Query",
    "UserType",
]
