# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import DocumentTag
from app.schema.queries.base import VersionedDjangoObjectType, LoginRequiredObjectType


class DocumentTagFilter(django_filters.FilterSet):
    # Fields for searching documentTags
    name = django_filters.CharFilter(field_name="name", distinct=True)


class DocumentTagType(VersionedDjangoObjectType):
    class Meta:
        model = DocumentTag
        fields = [
            "name",
            "parent",
        ]
        filterset_class = DocumentTagFilter
        interfaces = [relay.Node]


class DocumentTagQuery(LoginRequiredObjectType):
    document_tag = relay.Node.Field(DocumentTagType)
    all_document_tags = DjangoFilterConnectionField(DocumentTagType)


__all__ = ["DocumentTagType", "DocumentTagQuery"]
