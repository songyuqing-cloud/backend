# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene import Field, relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import Course, Employee, Faculty
from app.utils.search import course_code_name_search

from .base import LoginRequiredObjectType, VersionedDjangoObjectType


class CourseFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=course_code_name_search)
    order_by = django_filters.OrderingFilter(fields=["code", "name"])

    # Fields for searching courses
    faculty = django_filters.CharFilter(
        field_name="offerings__program__type__faculty__human_code", distinct=True
    )
    program = django_filters.CharFilter(
        field_name="offerings__program__code", distinct=True
    )
    year = django_filters.CharFilter(field_name="offerings__year", distinct=True)
    semester = django_filters.CharFilter(
        field_name="offerings__semester", distinct=True
    )
    code = django_filters.CharFilter(field_name="offerings__code", distinct=True)

    class Meta:
        model = Course
        fields = ["search", "order_by", "faculty", "program", "year", "semester"]

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if hasattr(queryset.first(), "similarity"):
            # Filter courses based on numeric threshold
            queryset = queryset.filter(similarity__gte=0.23)
            queryset = queryset.order_by("-similarity")[:40]
        return queryset


class CourseType(VersionedDjangoObjectType):
    main_faculty = graphene.Field("app.schema.queries.faculty.FacultyType")
    faculties = graphene.List("app.schema.queries.faculty.FacultyType")
    instructors = graphene.List("app.schema.queries.employee.EmployeeType")
    subscribed = graphene.Field(graphene.Boolean)

    @staticmethod
    def resolve_main_faculty(parent, info, **kwargs):
        """Return the main faculty this course is taught at.

        The first letter of the course code is the code of the faculty.
        """
        faculty_code = next(iter(parent.code), None)
        return Faculty.objects.filter(code=faculty_code).first()

    @staticmethod
    def resolve_faculties(parent, info, **kwargs):
        """Return all faculties this course is taught at."""
        faculty_pks = parent.offerings.values_list(
            "program__type__faculty__pk", flat=True
        )
        return Faculty.objects.filter(pk__in=faculty_pks)

    @staticmethod
    def resolve_instructors(parent, info, **kwargs):
        """Resolve the instructors that teach this course."""
        instructor_pks = parent.offerings.values_list("instructor__pk", flat=True)
        return Employee.objects.filter(pk__in=instructor_pks)

    @staticmethod
    def resolve_subscribed(parent, info, **kwargs):
        user = info.context.user
        return user in parent.subscriptions.all()

    class Meta:
        model = Course
        fields = [
            "code",
            "name",
            "specifications_url",
            "content",
            "subscriptions",
            "offerings",
            "documents",
        ]
        filterset_class = CourseFilter
        interfaces = [relay.Node]


class CourseQuery(LoginRequiredObjectType):
    course_by_id = relay.Node.Field(CourseType)
    course_by_code = Field(CourseType, code=graphene.String(required=True))
    course_by_name = Field(CourseType, name=graphene.String(required=True))
    all_courses = DjangoFilterConnectionField(CourseType)

    @staticmethod
    def resolve_course_by_code(parent, info, code):
        return Course.objects.filter(code=code).first()

    @staticmethod
    def resolve_course_by_name(parent, info, name):
        return Course.objects.filter(name=name).first()


__all__ = ["CourseType", "CourseQuery"]
