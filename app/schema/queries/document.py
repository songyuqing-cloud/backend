# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from django.db.models import Avg
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import Course, Document

from .base import LoginRequiredObjectType, VersionedDjangoObjectType


class DocumentFilter(django_filters.FilterSet):
    # TODO
    # search = django_filters.CharFilter(method=code_name_search)
    # order_by = django_filters.OrderingFilter(fields=["code", "name"])

    # Fields for searching documents
    course = django_filters.CharFilter(field_name="course", distinct=True)
    author = django_filters.CharFilter(field_name="author", distinct=True)

    class Meta:
        model = Course
        fields = [
            # "search", "order_by",
            "course",
            "author",
        ]


class DocumentType(VersionedDjangoObjectType):
    average_ratings = graphene.Field(graphene.Float)
    my_rating = graphene.Field(graphene.String)
    ratings_count = graphene.Field(graphene.Int)

    @staticmethod
    def resolve_average_ratings(parent, info, **kwargs):
        return parent.document_ratings.aggregate(Avg("rating"))["rating__avg"]

    @staticmethod
    def resolve_my_rating(parent, info, **kwargs):
        user = info.context.user
        if parent.document_ratings.filter(user_id=user.id):
            return parent.document_ratings.filter(user_id=user.id).first().rating
        return None

    @staticmethod
    def resolve_ratings_count(parent, info, **kwargs):
        return parent.document_ratings.count()

    class Meta:
        model = Document
        fields = [
            "course",
            "author",
            "name",
            "page_count",
            "download_count",
            "upload_date",
            "last_updated",
            "file",
            "tags",
            "document_ratings",
        ]
        filterset_class = DocumentFilter
        interfaces = [relay.Node]


class DocumentQuery(LoginRequiredObjectType):
    document = relay.Node.Field(DocumentType)
    all_documents = DjangoFilterConnectionField(DocumentType)


class DocumentExtensionsQuery(LoginRequiredObjectType):
    document_extensions = graphene.List(graphene.String)

    @staticmethod
    def resolve_document_extensions(parent, info, **kwargs):
        return Document.document_extensions


__all__ = ["DocumentType", "DocumentQuery", "DocumentExtensionsQuery"]
