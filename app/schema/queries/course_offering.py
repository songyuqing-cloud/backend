# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from app.models import Course, CourseOffering
from app.utils.search import course_code_name_search

from .base import LoginRequiredObjectType, VersionedDjangoObjectType


class CourseOfferingFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=course_code_name_search)
    order_by = django_filters.OrderingFilter(fields=["code", "name"])

    class Meta:
        model = Course
        fields = ["search", "order_by"]

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if hasattr(queryset.first(), "similarity"):
            queryset = queryset.order_by("-similarity")
        return queryset


class CourseOfferingType(VersionedDjangoObjectType):
    class Meta:
        model = CourseOffering
        fields = [
            "program",
            "course",
            "code",
            "reference",
            "semester",
            "year",
            "year_part_time",
            "department",
            "instructor",
            "contact_hours",
            "study_hours",
            "credits",
        ]
        filter_fields = {}
        interfaces = [relay.Node]


class CourseOfferingQuery(LoginRequiredObjectType):
    course_offering = relay.Node.Field(CourseOfferingType)
    all_course_offerings = DjangoFilterConnectionField(CourseOfferingType)
    all_years = graphene.List(graphene.String)
    all_semesters = graphene.List(graphene.String)

    @staticmethod
    @login_required
    def resolve_all_years(parent, info, **kwargs):
        # TODO: Allow filtering on faculty, program and year
        course_offerings = CourseOffering.objects.filter(year__isnull=False)
        return sorted(set(course_offerings.values_list("year", flat=True)))

    @staticmethod
    @login_required
    def resolve_all_semesters(parent, info, **kwargs):
        # TODO: Allow filtering on faculty, program and year
        course_offerings = CourseOffering.objects.filter(semester__isnull=False)
        return sorted(set(course_offerings.values_list("semester", flat=True)))


__all__ = ["CourseOfferingType", "CourseOfferingQuery"]
