# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import Course, ProgramType
from app.utils.search import code_name_search

from .base import LoginRequiredObjectType, VersionedDjangoObjectType
from .course import CourseType


class ProgramTypeFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=code_name_search)
    order_by = django_filters.OrderingFilter(fields=["code", "name"])

    class Meta:
        model = ProgramType
        fields = ["search", "order_by"]


class ProgramTypeType(VersionedDjangoObjectType):
    courses = DjangoFilterConnectionField(CourseType)

    @staticmethod
    def resolve_courses(parent, info, **kwargs):
        return Course.objects.filter(offerings__program__type__pk=parent.pk)

    class Meta:
        model = ProgramType
        fields = ["code", "name", "programs"]
        filterset_class = ProgramTypeFilter
        interfaces = [relay.Node]


class ProgramTypeQuery(LoginRequiredObjectType):
    program_type = relay.Node.Field(ProgramTypeType)
    all_program_types = DjangoFilterConnectionField(ProgramTypeType)


__all__ = ["ProgramTypeType", "ProgramTypeQuery"]
