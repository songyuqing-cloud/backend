# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import graphene
from graphene import relay

from app.models import User as UserModel

from .base import LoginRequiredObjectType, VersionedDjangoObjectType


class UserType(VersionedDjangoObjectType):
    class Meta:
        model = UserModel
        fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "is_staff",
            "date_joined",
            "faculty",
            "university_id",
            "last_enrolled",
            "subscriptions",
        ]
        filter_fields = ["username", "first_name", "last_name", "email"]
        interfaces = [relay.Node]


class UserQuery(LoginRequiredObjectType):
    me = graphene.Field(UserType)

    @staticmethod
    def resolve_me(parent, info, **kwargs):
        return getattr(info.context, "user", None)


__all__ = ["UserType", "UserQuery"]
