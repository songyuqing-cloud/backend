# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import DocumentRating
from app.schema.queries.base import LoginRequiredObjectType, VersionedDjangoObjectType


class DocumentRatingFilter(django_filters.FilterSet):
    # Fields for searching documentRatings
    user = django_filters.CharFilter(field_name="user", distinct=True)


class DocumentRatingType(VersionedDjangoObjectType):
    class Meta:
        model = DocumentRating
        fields = [
            "document",
            "user",
            "rating",
        ]
        filterset_class = DocumentRatingFilter
        interfaces = [relay.Node]


class DocumentRatingQuery(LoginRequiredObjectType):
    document_rating = relay.Node.Field(DocumentRatingType)
    all_document_ratings = DjangoFilterConnectionField(DocumentRatingType)


__all__ = ["DocumentRatingType", "DocumentRatingQuery"]
