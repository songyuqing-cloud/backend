# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import django_filters
import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField

from app.models import Course, Faculty
from app.utils.search import human_code_name_search

from .base import LoginRequiredObjectType, VersionedDjangoObjectType
from .course import CourseType


class FacultyFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method=human_code_name_search)
    order_by = django_filters.OrderingFilter(
        fields={"human_code": "code", "name": "name"}
    )

    class Meta:
        model = Faculty
        fields = ["search", "order_by"]


class FacultyType(VersionedDjangoObjectType):
    code = graphene.String(source="human_code")
    courses = DjangoFilterConnectionField(CourseType)

    @staticmethod
    def resolve_courses(parent, info, **kwargs):
        return Course.objects.filter(offerings__program__type__faculty__pk=parent.pk)

    class Meta:
        model = Faculty
        fields = ["name", "program_types"]
        filterset_class = FacultyFilter
        interfaces = [relay.Node]


class FacultyQuery(LoginRequiredObjectType):
    faculty = relay.Node.Field(FacultyType)
    all_faculties = DjangoFilterConnectionField(FacultyType)


__all__ = ["FacultyType", "FacultyQuery"]
