# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from django.db.models import Q
from django.db.models.functions import Greatest
from django.contrib.postgres.search import TrigramSimilarity


def code_name_search(queryset, name, value):
    return queryset.annotate(similarity=TrigramSimilarity("name", value)).filter(
        Q(code__istartswith=value) | Q(similarity__gte=0.2)
    )


def course_code_name_search(queryset, name, value):
    value = value.strip()

    coursecode_queryset = queryset.filter(code=value)
    # An exact match with the course code is found
    if coursecode_queryset:
        queryset = coursecode_queryset
    # No exact match is found
    else:
        # Compute TrigramSimilarity for the course names and instructor names
        queryset = queryset.annotate(NameSimilarity=TrigramSimilarity("name", value))
        queryset = queryset.annotate(
            InstructorSimilarity=TrigramSimilarity("offerings__instructor__name", value)
        ).distinct()
        # Similarity score is the max of NameSimilarity and InstructorSimilarity
        queryset = queryset.annotate(
            similarity=Greatest("NameSimilarity", "InstructorSimilarity")
        )
    return queryset


def human_code_name_search(queryset, name, value):
    return queryset.filter(Q(human_code__istartswith=value) | Q(name__icontains=value))
