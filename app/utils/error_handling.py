# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)
from graphql import GraphQLError


def vuetifize_variable(python_string):
    """
    Convert variable names to error handling names.

    Args:
        python_string: Variable name of the object which causes an error

    Returns:
        Name compatible with our Vue error-styling
    """
    words_list = python_string.split("_")
    vuetifized_word = ""
    for word in words_list:
        vuetifized_word += word.title()
    return vuetifized_word


def studium_error(error_object: str, error_message: str):
    """
    Convention for error handling in Studium.

    Args:
        error_object: Name of the object that causes an error
        error_message: Message to be displayed

    Returns:
        A JSON-parsable error-string
    """
    return GraphQLError('{"%s": "%s"}' % (error_object + "Errors", error_message))


def studium_clean(parent_object, **local_vars):
    r"""
    Raise errors and handle clean-functions.

    Args:
        parent_object: MutationObject-Type or QueryObject-Type
        \*\*local_vars: All local objects; Hint: "\*\*locals()"

    Returns:
        Void
        Function raises errors
    """
    clean_functions = [
        func
        for func in dir(parent_object)
        if callable(getattr(parent_object, func)) and "clean" in func
    ]
    mutation_inputs = [
        func
        for func in dir(parent_object.Arguments)
        if not callable(getattr(parent_object.Arguments, func))
        and not func.startswith("__")
    ]

    for clean_function in clean_functions:
        used_mutation_inputs = []
        for mutation_input in mutation_inputs:
            if mutation_input in clean_function:
                used_mutation_inputs += [mutation_input]

        if len(used_mutation_inputs) == 1:
            return_value = getattr(parent_object, clean_function)(
                local_vars.get(used_mutation_inputs[0])
            )

            if return_value:
                raise studium_error(
                    vuetifize_variable(used_mutation_inputs[0]), return_value
                )
        elif len(used_mutation_inputs) == 0:
            return_value = getattr(parent_object, clean_function)(
                {
                    mutation_input: local_vars.get(mutation_input)
                    for mutation_input in mutation_inputs
                }
            )

            if return_value:
                raise studium_error("General", return_value)
        else:
            return_value = getattr(parent_object, clean_function)(
                {
                    mutation_input: local_vars.get(mutation_input)
                    for mutation_input in used_mutation_inputs
                }
            )

            if return_value:
                raise studium_error("General", return_value)
