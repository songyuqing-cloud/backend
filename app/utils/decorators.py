# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from functools import wraps


def unique_boolean(field, subset=None):
    """Allow to specify a unique boolean for a model."""

    def cls_factory(cls):
        def factory(func):
            @wraps(func)
            def decorator(self, *args, **kwargs):
                kwargs = {field: True}
                for arg in subset or []:
                    if getattr(self, arg):
                        kwargs[arg] = getattr(self, arg)
                instances = self.__class__.objects.filter(**kwargs)
                if getattr(self, field):
                    instances.exclude(pk=self.pk).update(**{field: False})
                elif instances.count() == 0:
                    setattr(self, field, True)
                return func(self)

            return decorator

        if hasattr(cls, "save"):
            cls.save = factory(cls.save)
        return cls

    return cls_factory


__all__ = ["unique_boolean"]
