SHELL := /bin/bash

all: up logs
.PHONY: all

#####################
### Configuration ###
#####################
# Make docker-compose use the docker command-line interface for BuildKit support
export COMPOSE_DOCKER_CLI_BUILD := 1
# Use BuildKit to optimize builds
export DOCKER_BUILDKIT := 1
# Make sure to use the OverlayFS driver
export DOCKER_DRIVER := overlay2

# Get cache from development by default
export CI_PROJECT_PATH ?= vtkgent/site
export CI_REGISTRY_IMAGE ?= registry.gitlab.com/$(CI_PROJECT_PATH)
# Build development by default
export TARGET ?= development
export VERSION ?= $(TARGET)
export CACHE_VERSION ?= $(VERSION)

###################
### Credentials ###
###################
docker-login:
	@ echo "Validating docker login ..."
	@ { grep "registry.gitlab.com" "$(HOME)/.docker/config.json" && docker login registry.gitlab.com; } > /dev/null 2>&1 || { echo -e "\033[0;31merror: Docker is not logged in, please run 'docker login registry.gitlab.com'.\033[0m" && false; }
	@ echo -e "\033[0;32mDocker login valid!\033[0m"
.PHONY: docker-login

###############
### Targets ###
###############

build: docker-login
	@ # Fetch the latest image and do not fail if not found
	@ docker pull registry.gitlab.com/$(CI_PROJECT_PATH):$(CACHE_VERSION) || true
	@ # Build the new image with cache from the most recent image
	@ docker-compose build backend
.PHONY: build

publish: build
	@ docker-compose push backend
.PHONY: publish

up: build
	@ docker-compose up -d
.PHONY: up

logs:
	@ docker-compose logs -f backend
.PHONY: logs

down:
	@ docker-compose down $(ARGS)
.PHONY: down

prune:
	@ docker image prune -af
.PHONY: prune

check: build
	@ docker-compose run --rm backend pip-check -H
.PHONY: lint

initdb:
	@ docker-compose exec backend python manage.py initdb
.PHONY: init

messages: build
	@ docker-compose run --rm backend bash -c "python manage.py makemessages -l nl -i 'venv/*' && python manage.py compilemessages -l nl -i 'venv/*'"
.PHONY: messages

lint: build
ifeq ($(CI_JOB_ID),)
	@ docker-compose run --rm backend flake8 app
else
	@ docker-compose run --rm -v $(PWD)/out:/backend/out backend bash -c 'flake8 app --output-file="out/flake8.txt"; status="$$?"; flake8_junit "out/flake8.txt" "out/flake8.xml"; exit "$$status"'
endif
.PHONY: lint

fix-lint: build
	@ docker-compose run --rm backend black app
.PHONY: fix-lint

test: build
	@ $(eval CMD := python -m coverage run manage.py test --force-color --configuration="Testing" --verbosity 1 --parallel --keepdb)
ifeq ($(CI_JOB_ID),)
	@ docker-compose run --rm backend $(CMD)
else
	@ docker-compose run --rm -v $(PWD)/out:/backend/out backend bash -c '$(CMD); status="$$?"; python -m coverage xml; exit "$$status"'
endif
.PHONY: test

clean:
	@ $(MAKE) prune
.PHONY: clean
