##################
### Base image ###
##################
FROM python:3.9-slim as base

# Disable bufferering
ENV PYTHONUNBUFFERED=1

# Set default enviroment to development
ENV DJANGO_CONFIGURATION=Development

# Set working directory
WORKDIR /app

# Set the port that this container exposes
EXPOSE 80/tcp

# Install gcc
RUN apt-get update && apt-get install -y \
    gcc \
    gettext \
    && rm -rf /var/lib/apt/lists/*

# Copy configuration
COPY pyproject.toml setup.cfg setup.py ./

# Install required packages
RUN pip install -e .

#########################
### Development image ###
#########################
FROM base as development

# Install development packages
RUN pip install -e .[development]

# Copy source
COPY . .

# Start development server
CMD python manage.py migrate && python manage.py runserver 0.0.0.0:80

########################
### Production image ###
########################
FROM base as production

# Install gettext and postgresql
RUN apt-get update && apt-get install -y \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# Install production packages
RUN pip install -e .[production]

# Copy source
COPY . .

# Collect static files
RUN python manage.py collectstatic --noinput

# Compile translations
RUN python manage.py compilemessages

# Set enviroment to production
ENV DJANGO_CONFIGURATION=Production

# Start production server
CMD python manage.py check --deploy \
    && python manage.py migrate \
    && gunicorn \
    --bind=0.0.0.0:80 \
    --workers=5 \
    --worker-tmp-dir=/dev/shm \
    --timeout 300 \
    --log-file=- \
    --access-logfile=/var/log/access.log \
    --error-logfile=/var/log/error.log \
    --log-level info \
    app.wsgi:application
