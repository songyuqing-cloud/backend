# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from collections import defaultdict

from app.models import Faculty
from scraper.items import FacultyItem

from .base import BasePipeline


class FacultyPipeline(BasePipeline):
    """A class that is responsible of processing faculty items that are crawled by spiders.

    The data for the faculties are spread over multiple pages, so this pipeline will
    merge the data before making persistent changes to the database.
    """

    def open_spider(self, spider):
        """Initialize a dictionary that will contain the deferred faculty items."""
        self.deferred = defaultdict(dict)

    def close_spider(self, spider):
        """Create or update all faculties from the deferred codes.

        Args:
            spider: A reference to the spider.
        """
        for name, attributes in self.deferred.items():
            self.create_or_update_from_item(
                Faculty, name__iexact=name, values=attributes
            )

    def process_item(self, item, spider):
        """Add a new faculty item to the dictionary.

        Multiple faculty items can be crawled, each containing different codes for a
        faculty. This function defers the completion of codes that are not found in the
        Course Catalogue to to the point where the spider is closed. This makes sure
        that name of a faculty has the same case as the name in the course catalogue.

        Args:
            item: The faculty to be processed.
            spider: A reference to the spider.

        Returns:
            DjangoItem: The crawled item to be passed on to the next pipeline.
        """
        if not isinstance(item, FacultyItem):
            return item

        self.deferred[item["name"].lower()].update({k: v for k, v in item.items() if v})
        return item


__all__ = ["FacultyPipeline"]
