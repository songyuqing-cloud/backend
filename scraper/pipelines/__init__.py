# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from .course import CoursePipeline
from .faculty import FacultyPipeline

__all__ = ["FacultyPipeline", "CoursePipeline"]
