# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)


class BasePipeline:
    @staticmethod
    def create_or_update_from_item(cls, values=None, **kwargs):
        field_names = [field.name for field in cls._meta.get_fields()]
        values = {k: v for k, v in (values or {}).items() if k in field_names}
        values.update(kwargs)

        obj, created = cls.objects.get_or_create(**kwargs, defaults=values)
        if not created:
            obj.update(**values)
        return obj
