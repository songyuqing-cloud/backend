# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from app.models import Course, CourseOffering, Employee, Faculty, Program, ProgramType
from scraper.items import CourseOfferingItem

from .base import BasePipeline


class CoursePipeline(BasePipeline):
    @classmethod
    def process_item(cls, item, spider):
        if not isinstance(item, CourseOfferingItem):
            return item

        # Unwind item
        instructor_item = item["instructor_item"]
        course_item = item["course_item"]
        program_item = item["program_item"]
        program_type_item = program_item["type_item"]
        faculty_item = program_type_item["faculty_item"]

        # Get or create faculty
        faculty = cls.create_or_update_from_item(
            Faculty, code=faculty_item["code"], values=faculty_item
        )

        # Get or create program type
        program_type_item["faculty"] = faculty
        program_type = cls.create_or_update_from_item(
            ProgramType,
            code=program_type_item["code"],
            values=program_type_item,
        )

        # Get or create program type
        program_item["type"] = program_type
        program = cls.create_or_update_from_item(
            Program,
            code=program_item["code"],
            values=program_item,
        )

        # Get or create course
        course = cls.create_or_update_from_item(
            Course, code=course_item["code"], values=course_item
        )

        # Get or create instructor
        instructor = (
            cls.create_or_update_from_item(
                Employee, code=instructor_item["code"], values=instructor_item
            )
            if instructor_item
            else None
        )

        # Get or create course offering
        item["instructor"] = instructor
        cls.create_or_update_from_item(
            CourseOffering,
            program=program,
            course=course,
            values=item,
        )

        return item


__all__ = ["CoursePipeline"]
