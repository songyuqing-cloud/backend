# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import scrapy

from scraper.items import FacultyItem


class FacultySpider(scrapy.Spider):
    """This spider crawls identification codes for faculties.

    Each different code for a faculty is found on a different website, requiring us to
    yield multiple items for every single faculty. These different items will be merged
    in the FacultyPipeline.
    """

    name = "faculties"

    # XPath query definitions
    FACULTIES_BY_CODE_XPATH = '//a[@href="../FACULTY/faculteiten.html"]/../ul//a'
    FACULTIES_BY_HUMAN_CODE_XPATH = (
        '//a[contains(@class, "internal-link") '
        'and starts-with(@href,"https://www.ugent.be/")]'
    )
    FACULTIES_BY_LEGACY_CODE_XPATH = (
        '//a[starts-with(@href, "https://telefoonboek.ugent.be/nl/faculties/")]'
    )

    def start_requests(self):
        """Start the spider at the different urls where the codes can be found."""
        yield scrapy.Request(
            "https://studiegids.ugent.be/2020/NL/FACULTY/faculteiten.html",
            self.parse_codes,
        )
        yield scrapy.Request(
            "https://www.ugent.be/nl/univgent/contact-adressen/faculteiten",
            self.parse_human_codes,
        )
        yield scrapy.Request(
            "https://telefoonboek.ugent.be/nl/faculties", self.parse_legacy_codes
        )

    def parse_codes(self, response, **kwargs):
        """Parse the identification codes for the faculties."""
        for e in response.xpath(self.FACULTIES_BY_CODE_XPATH):
            yield FacultyItem(code=self.__get_code(e), name=self.__get_name(e))

    def parse_human_codes(self, response, **kwargs):
        """Parse the human readable codes for the faculties."""
        for e in response.xpath(self.FACULTIES_BY_HUMAN_CODE_XPATH):
            yield FacultyItem(
                human_code=f"F{self.__get_code(e)}", name=self.__get_name(e)
            )

    def parse_legacy_codes(self, response, **kwargs):
        """Parse the legacy codes for the faculties."""
        for e in response.xpath(self.FACULTIES_BY_LEGACY_CODE_XPATH):
            yield FacultyItem(legacy_code=self.__get_code(e), name=self.__get_name(e))

    @staticmethod
    def __get_code(e):
        """Extract and clean a code from an HTML tag."""
        href = e.xpath("@href").get()
        return href.rsplit("/", 1)[-1].strip().upper() if href else None

    @staticmethod
    def __get_name(e):
        """Extract and clean a name from an HTML tag."""
        text = e.xpath("text()").get()
        return text.strip() if text else None


__all__ = ["FacultySpider"]
