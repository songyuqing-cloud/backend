# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import re

import scrapy

from scraper.items import (
    CourseItem,
    CourseOfferingItem,
    EmployeeItem,
    FacultyItem,
    ProgramItem,
    ProgramTypeItem,
)


class CourseSpider(scrapy.Spider):
    name = "courses"
    start_urls = [
        "https://studiegids.ugent.be/2020/NL/FACULTY/faculteiten.html",
    ]

    # XPath query definitions
    FACULTIES_XPATH = '//a[@href="../FACULTY/faculteiten.html"]/../ul//a'
    PROGRAM_TYPE_XPATH = '//span[contains(@class, "opleidingssoort")]/..'
    PROGRAM_XPATH = "//li/a"
    PROGRAM_COURSES_XPATH = '//iframe[@id="hoofdframe"]/@src'

    def parse(self, response, **kwargs):
        for e in response.xpath(self.FACULTIES_XPATH):
            code = e.xpath("@href").get()
            href = f"{code}/opleidingstypes.html"
            meta = {
                "faculty": FacultyItem(
                    code=code.upper() if code else None, name=e.xpath("text()").get()
                )
            }
            yield response.follow(href, meta=meta, callback=self.parse_faculty)

    def parse_faculty(self, response):
        for e in response.xpath(self.PROGRAM_TYPE_XPATH):
            # Parse javascript strings out of onclick attribute
            code, href = e.xpath("@onclick").get().split("'")[1::2]
            faculty = response.meta["faculty"]
            meta = {
                "program_type": ProgramTypeItem(
                    code=code,
                    name=e.xpath("span/text()").get(),
                    faculty_item=faculty,
                )
            }
            yield response.follow(href, meta=meta, callback=self.parse_program_type)

    def parse_program_type(self, response):
        for e in response.xpath(self.PROGRAM_XPATH):
            href = e.xpath("@href").get()
            program_type = response.meta["program_type"]
            meta = {
                "program": ProgramItem(
                    code=next(iter(re.findall(r"(?<=/)\w+(?=\.html)", href)), None),
                    name=re.sub(r"\s+", " ", e.xpath("text()").get() or ""),
                    type_item=program_type,
                )
            }
            yield response.follow(href, meta=meta, callback=self.parse_program)

    def parse_program(self, response):
        href = response.xpath(self.PROGRAM_COURSES_XPATH).get()
        meta = response.meta
        yield response.follow(href, meta=meta, callback=self.parse_course_offerings)

    def parse_course_offerings(self, response) -> None:
        """Parse a response that has a list of CourseOfferings.

        This function cleans the data on the page and ignores courses that are
        irrelevant (not given or no specifications). An example of such an irrelevant
        course is 'Gezondheidspsychologie I' that can be found at
        https://studiegids.ugent.be/2020/NL/FACULTY/H/MABA/HMPSYCON/HMPSYCON.html.

        TODO: Explanation about how each field is parsed.

        Notes:
            Inline frames (with course offerings) are requested again and they are
            parsed with this method in turn.

        Args:
            response: The response that contains the html with the list of offerings.
        """
        for e in response.xpath(
            '//tr[contains(@class, "rowclass") '
            'and not(contains(@class, "nietgegeven"))]'
        ):
            course_url = e.xpath('td[@class="cursus"]/a/@href').get()
            if not course_url:
                continue
            course_url = course_url.strip() if course_url else None

            course_code = next(
                iter(re.findall(r"(?<=/)\w+(?=\.pdf)", course_url)), None
            )
            course_name = e.xpath('td[@class="cursus"]/a/text()').get()
            course_name = course_name.strip() if course_name else None

            semester_raw = e.xpath('td[@class="semester"]/text()').get()
            matches = re.match(r"(?<!\()((?P<c>\w+):)?(?P<s>\d+)(?!\))", semester_raw)
            code, semester = matches.group("c", "s") if matches else (None, None)

            year = e.xpath('td[@class="mt1"]/text()').get()
            year = year.strip() if year else None

            year_part_time = e.xpath('td[@class="mt2"]/text()').get()
            year_part_time = year_part_time.strip() if year_part_time else None

            department = e.xpath('td[@class="vakgroep"]/text()').get()
            department = department.strip() if department else department

            instructor_url = e.xpath('td[@class="lesgever"]/a/@href').get()
            instructor_url = instructor_url.strip() if instructor_url else None

            instructor_code = (
                next(iter(re.findall(r"(?<=/)\w+$", instructor_url)), None)
                if instructor_url
                else None
            )
            instructor_code = instructor_code.strip() if instructor_code else None

            instructor_name = (
                e.xpath('td[@class="lesgever"]/a/text()').get()
                if instructor_url
                else e.xpath('td[@class="lesgever"]/text()').get()
            )
            instructor_name = instructor_name.strip() if instructor_name else None

            credits = e.xpath('td[@class="studiepunten"]/text()').get()
            credits = credits.strip() if credits else None

            program_item = response.meta["program"]

            course_item = CourseItem(
                code=course_code,
                name=course_name,
                specifications_url=course_url,
                content="",
            )

            # TODO: category from url
            instructor_item = (
                EmployeeItem(code=instructor_code or "", name=instructor_name)
                if instructor_name
                else None
            )

            # TODO: reference, contact_hours and study_hours
            course_offering = CourseOfferingItem(
                program_item=program_item,
                course_item=course_item,
                code=code or "",
                semester=semester or "",
                year=year,
                year_part_time=year_part_time,
                department=department or "",
                instructor_item=instructor_item,
                credits=credits,
            )
            yield course_offering

        for iframe_href in response.xpath("//iframe/@src").getall():
            next_url = response.urljoin(iframe_href)
            yield response.follow(
                next_url, meta=response.meta, callback=self.parse_course_offerings
            )


__all__ = ["CourseSpider"]
