# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import os
import sys

import django

# Django setup
sys.path.insert(0, os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "app.settings"
os.environ["SCRAPY_SETTINGS_MODULE"] = "app.settings"
django.setup()
