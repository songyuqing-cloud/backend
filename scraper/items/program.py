# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import scrapy
from scrapy_djangoitem import DjangoItem

from app.models import Program, ProgramType


class ProgramItem(DjangoItem):
    django_model = Program
    type_item = scrapy.Field()


class ProgramTypeItem(DjangoItem):
    django_model = ProgramType
    faculty_item = scrapy.Field()


__all__ = ["ProgramItem", "ProgramTypeItem"]
