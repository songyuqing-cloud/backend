# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import scrapy
from scrapy_djangoitem import DjangoItem

from app.models import Course, CourseOffering


class CourseItem(DjangoItem):
    django_model = Course


class CourseOfferingItem(DjangoItem):
    django_model = CourseOffering
    program_item = scrapy.Field()
    course_item = scrapy.Field()
    instructor_item = scrapy.Field()


__all__ = ["CourseItem", "CourseOfferingItem"]
