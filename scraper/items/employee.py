# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

from scrapy_djangoitem import DjangoItem

from app.models import Employee


class EmployeeItem(DjangoItem):
    django_model = Employee


__all__ = ["EmployeeItem"]
