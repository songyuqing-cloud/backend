app.factories package
=====================

Submodules
----------

app.factories.base module
-------------------------

.. automodule:: app.factories.base
   :members:
   :undoc-members:
   :show-inheritance:

app.factories.module module
---------------------------

.. automodule:: app.factories.module
   :members:
   :undoc-members:
   :show-inheritance:

app.factories.page module
-------------------------

.. automodule:: app.factories.page
   :members:
   :undoc-members:
   :show-inheritance:

app.factories.user module
-------------------------

.. automodule:: app.factories.user
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.factories
   :members:
   :undoc-members:
   :show-inheritance:
