app.schema.mutations package
============================

Submodules
----------

app.schema.mutations.obtain\_jwt module
---------------------------------------

.. automodule:: app.schema.mutations.obtain_jwt
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.schema.mutations
   :members:
   :undoc-members:
   :show-inheritance:
