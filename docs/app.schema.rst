app.schema package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.schema.mutations
   app.schema.queries

Submodules
----------

app.schema.decorators module
----------------------------

.. automodule:: app.schema.decorators
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.schema
   :members:
   :undoc-members:
   :show-inheritance:
