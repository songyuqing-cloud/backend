app.schema.queries package
==========================

Submodules
----------

app.schema.queries.module module
--------------------------------

.. automodule:: app.schema.queries.module
   :members:
   :undoc-members:
   :show-inheritance:

app.schema.queries.page module
------------------------------

.. automodule:: app.schema.queries.page
   :members:
   :undoc-members:
   :show-inheritance:

app.schema.queries.user module
------------------------------

.. automodule:: app.schema.queries.user
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app.schema.queries
   :members:
   :undoc-members:
   :show-inheritance:
