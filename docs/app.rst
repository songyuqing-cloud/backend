app package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   app.admin
   app.factories
   app.models
   app.schema
   app.tests
   app.utils

Submodules
----------

app.asgi module
---------------

.. automodule:: app.asgi
   :members:
   :undoc-members:
   :show-inheritance:

app.settings module
-------------------

.. automodule:: app.settings
   :members:
   :undoc-members:
   :show-inheritance:

app.tests module
----------------

.. automodule:: app.tests
   :members:
   :undoc-members:
   :show-inheritance:

app.urls module
---------------

.. automodule:: app.urls
   :members:
   :undoc-members:
   :show-inheritance:

app.wsgi module
---------------

.. automodule:: app.wsgi
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: app
   :members:
   :undoc-members:
   :show-inheritance:
