#!/usr/bin/env python
# Copyright: (c) 2020-2021, VTK Gent vzw
# GNU Affero General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

import setuptools

if __name__ == "__main__":
    setuptools.setup()
